#!/bin/bash

# Cambodia 2018 ITS
### Anne-Sophie Masson
### 8 July 2020

############## Config SLURM ##############
## Partition definition
#SBATCH -p highmem
## Node definition
#SBATCH --nodelist=node17
## Job name definition
#SBATCH --job-name=ITS_part3
## Email sending
#SBATCH --mail-user=annesophie.masson@ird.fr
## Email options
#SBATCH --mail-type=ALL
##########################################

############## Variables declaration ##############
path_to_dir="/home/masson/THESE/DATA_Cambodge/BackToTheCluster_analyses/ITS/Part2";
path_to_tmp="/scratch/masson_ITS";
path_to_results="/home/masson/THESE/DATA_Cambodge/BackToTheCluster_analyses/ITS/Part3"

# 0. Preparation (skip this step if the outputs of part 2 are already in the scratch)

## Delete the tmp repository
rm -r $path_to_tmp
echo "If no error displayed - TMP REPO IN THE NODE HAS BEEN DELETED FOR NEW ANALYSIS"

## Copy the data in the scratch
mkdir $path_to_tmp
cd $path_to_dir
scp -r nas:$path_to_dir/Cambodia_2018_ITS-rep-seqs.qza $path_to_tmp
scp -r nas:$path_to_dir/Cambodia_2018_ITS_table-4-filterlowab-FINAL.qza $path_to_tmp
scp -r nas:$path_to_dir/Cambodia_2018_ITS_table-4-filterlowab-FINAL.qzv $path_to_tmp
scp -r nas:/home/masson/THESE/DATA_Cambodge/Cambodia_2018_metadata.txt  $path_to_tmp
echo "If no error displayed - NEEDED FILES HAVE BEEN TRANSFERED FROM MASTER (HOME) -> NODE (TMP SCRATCH)"
cd $path_to_tmp

## Load software
module load bioinfo/qiime2/2020.2
source activate qiime2-2020.2

########### Start the analysis ###########

# 7. Check rarefaction curves and rarefy dataset

## Check rarefaction curves - change --p-min-depth and --pmax-depth depending on the dataset
qiime diversity alpha-rarefaction \
  --i-table Cambodia_2018_ITS_table-4-filterlowab-FINAL.qza \
  --m-metadata-file Cambodia_2018_metadata.txt \
  --o-visualization Cambodia_2018_ITS_alpha_rarefaction_curves.qzv \
  --p-min-depth 712 \
  --p-max-depth 15611
echo "If no error displayed - RAREFACTION CURVE FILE HAS BEEN CREATED (QZV CREATED)"

qiime diversity alpha-rarefaction \
   --i-table Cambodia_2018_ITS_table-4-filterlowab-FINAL.qza \
   --p-max-depth 15611 \
   --p-steps 20 \
   --o-visualization Cambodia_2018_ITS_rarefaction_curves_eachsample.qzv
echo "If no error displayed - RAREFACTION CURVES FOR EACH SAMPLE HAVE BEEN CREATED (QZV CREATED)"

### Choose rarefaction level and rarefy the table
# It is not necessary to rarefy our data since the sequencing depth is enough but it is recommended as one way to normalize the data in order to calculate alpha diversity indices.
qiime feature-table rarefy \
  --i-table Cambodia_2018_ITS_table-4-filterlowab-FINAL.qza \
  --p-sampling-depth 712 \
  --o-rarefied-table Cambodia_2018_ITS_table-FINAL-rarefied.qza
echo "If no error displayed - FINAL TABLE HAS BEEN RAREFIED (QZA CREATED)"

mv Cambodia_2018_ITS_table-4-filterlowab-FINAL.qza Cambodia_2018_ITS_table-FINAL-unrarefied.qza
echo "If no error displayed - WE HAVE RAREFIED AND UNRAREFIED TABLES (QZA)"

# 8. Make summary after rarefaction
qiime feature-table summarize \
  --i-table Cambodia_2018_ITS_table-FINAL-rarefied.qza \
  --o-visualization Cambodia_2018_ITS_table-FINAL-rarefied.qzv \
  --m-sample-metadata-file Cambodia_2018_metadata.txt
  
mv Cambodia_2018_ITS_table-4-filterlowab-FINAL.qzv Cambodia_2018_ITS_table-FINAL-unrarefied.qzv
echo "If no error displayed - CORRESPONDING QZV FILES HAVE BEEN CREATED"

# 9. Export SV tables, rep seqs and taxonomy

## Filter the rep-seqs.qza to keep only SVs that are present in the rarefied SV table 
qiime feature-table filter-seqs \
  --i-data Cambodia_2018_ITS-rep-seqs.qza \
  --i-table Cambodia_2018_ITS_table-FINAL-rarefied.qza \
  --o-filtered-data Cambodia_2018_ITS-rep-seqs-FINAL-rarefied.qza
echo "If no error displayed - CORRESPONDING FINAL REP SEQ HAS BEEN RAREFIED (QZA CREATED)"
  
mv Cambodia_2018_ITS-rep-seqs.qza Cambodia_2018_ITS-rep-seqs-FINAL-unrarefied.qza
echo "If no error displayed - WE HAVE RAREFIED AND UNRAREFIED REP SEQS (QZA)"

## Export the rep-seqs-FINAL for rarefied and unrarefied datasets
qiime tools export \
   --input-path Cambodia_2018_ITS-rep-seqs-FINAL-rarefied.qza \
   --output-path ITS_Final_output_files
   
mv ITS_Final_output_files Cambodia_2018_ITS-rep-seqs-FINAL-rarefied

qiime tools export \
   --input-path Cambodia_2018_ITS-rep-seqs-FINAL-unrarefied.qza \
   --output-path ITS_Final_output_files
   
mv ITS_Final_output_files Cambodia_2018_ITS-rep-seqs-FINAL-unrarefied

echo "If no error displayed - WE HAVE EXPORTED THE REP SEQS"

## Export SV tables for rarefied and unrarefied datasets 
qiime tools export \
  --input-path Cambodia_2018_ITS_table-FINAL-rarefied.qza \
  --output-path ITS_Final_output_files
  
mv ITS_Final_output_files Cambodia_2018_ITS-table-FINAL-rarefied

qiime tools export \
  --input-path Cambodia_2018_ITS_table-FINAL-unrarefied.qza \
  --output-path ITS_Final_output_files
  
mv ITS_Final_output_files Cambodia_2018_ITS-table-FINAL-unrarefied

echo "If no error displayed - WE HAVE EXPORTED THE TABLE"

## Convert to biom format
cd Cambodia_2018_ITS-table-FINAL-rarefied
biom convert \
   -i feature-table.biom \
   -o Cambodia_2018_ITS-table-FINAL-rarefied.txt \
   --to-tsv
   
cd ../Cambodia_2018_ITS-table-FINAL-unrarefied
biom convert \
   -i feature-table.biom \
   -o Cambodia_2018_ITS-table-FINAL-unrarefied.txt \
   --to-tsv

echo "If no error displayed - WE HAVE CONVERTED RESULTS TO TXT FORMAT"

########### End of the analysis ###########

## Copy the results in the home
scp -r $path_to_tmp/* nas:$path_to_results
echo "If no error displayed - RESULTS HAVE BEEN TRANSFERED FROM NODE (TMP SCRATCH) -> MASTER (HOME)"

## Delete the tmp repository
#rm -r $path_to_tmp
#echo "If no error displayed - TMP REPO IN THE NODE HAS BEEN DELETED FOR NEW ANALYSIS"

echo "The END"
# Time to complete: 1min19
